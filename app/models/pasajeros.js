var models = require('./models'),
	Schema = models.Schema;

var pasajeroSchema = new Schema({
	nombres : String,
	apellidos : String,
	correo : String,
	telefono : String,
  location : { type: { type: String }, coordinates: [Number]},
	referencia: String,
	conductor: {type : Schema.ObjectId, ref : 'Conductor'},
});

var Pasajero = models.model('Pasajero', pasajeroSchema, 'pasajeros');

module.exports = Pasajero;