const models = require('./models');
let Schema = models.Schema;

const cuadranteSchema = new Schema({
	nombre : String,
	direccion : String,
	location : { type: { type: String }, coordinates: [Number]}
});

cuadranteSchema.index({location : '2dsphere'});
const Cuadrante = models.model('Cuadrante', cuadranteSchema, 'cuadrantes');

module.exports =  Cuadrante;