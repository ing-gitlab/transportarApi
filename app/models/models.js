var mongoose = require('mongoose');

// mongoose.connect('mongodb://localhost/transportar');
mongoose.connect('mongodb://transportar:transportarctg@ds135552.mlab.com:35552/transportar');
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
  console.log('database connection established successfully');
});

module.exports = mongoose;