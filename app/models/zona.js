var models = require('./models'),
	Schema = models.Schema;

var zonaSchema = new Schema({
	nombre : String,
	descripcion : String
	
});

var Zona = models.model('Zona', zonaSchema, 'zonas');

module.exports = Zona;