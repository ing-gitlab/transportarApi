var models = require('./models'),
	Schema = models.Schema;

var deviceSchema = new Schema({
	registrationId: String,
	location : { type: { type: String }, coordinates: [Number]}
});

var Device = models.model('Device', deviceSchema, 'devices');

module.exports = Device;
