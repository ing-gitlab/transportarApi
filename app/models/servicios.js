var models = require('./models'),
	Schema = models.Schema;

var servicioSchema = new Schema({
	pasajero : { type : Schema.ObjectId, ref : 'Pasajero' },
	conductor : { type : Schema.ObjectId, ref : 'Conductor' },
	location : { type: { type: String }, coordinates: [Number]},
	fecha : String,
	estado: String
});

var Servicio = models.model('Servicio', servicioSchema, 'servicios');

module.exports = Servicio;
