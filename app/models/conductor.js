var models = require('./models'),
	Schema = models.Schema;

var conductorSchema = new Schema({
	device : {type : Schema.ObjectId, ref : 'Device'},
	nombres : String,
	apellidos : String,
	correo : String,
	password : String,
	telefono : String,
	cuadrante : String,
	estado : {type : String, default : 'Inactivo'}
});

var Conductor = models.model('Conductor', conductorSchema, 'conductores');

module.exports = Conductor;
