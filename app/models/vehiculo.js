var models = require('./models'),
	Schema = models.Schema;

var vehiculoSchema = new Schema({
	placa : String,
	cupos : [{type : Schema.ObjectId, ref : 'Estudiante'}],
	zona : {type : Schema.ObjectId, ref : 'Zona'}

});

var Vehiculo = models.model('Vehiculo', vehiculoSchema, 'vehiculos');

module.exports = Vehiculo;