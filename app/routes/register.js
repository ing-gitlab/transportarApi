module.exports = (app) => {
	require('./../controllers/devices')(app);
	require('./../authentication/local')();
	require('./../controllers/index')(app);
	require('./../controllers/cuadrantes')(app);
	require('./../controllers/conductores')(app);
	require('./../controllers/servicios')(app);
	require('./../controllers/pasajeros')(app);
	require('./../controllers/vehiculos')(app);
	require('./../controllers/zonas')(app);
	require('./../default/data')();
};
