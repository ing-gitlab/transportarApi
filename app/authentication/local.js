const passport = require('passport');
const { Strategy } = require('passport-local');
const passwordHash = require('password-hash');
const Conductor = require('../models/conductor');
let LocalStrategy = Strategy;

module.exports = () => {
	passport.serializeUser((conductor, done) => {
		done(null, conductor);
	});

	passport.deserializeUser((id, done) => {
		Conductor.findOne({ _id: id })
		.exec((err, conductor) => {
			done(null, conductor);
		});
	});

	passport.use('user', new LocalStrategy({
		usernameField : 'correo',
		passwordFiel : 'password'
	}, (correo, password, done) => {

		Conductor.findOne({ correo : correo }).exec((err, conductor) => {
			if (err) return done(null, false);
			if (!conductor)
				return done(null, false);
			if (!passwordHash.verify(password, conductor.password))
				return done(null, false);

			return done(null, conductor);
		});
	}));
};
