const Servicios = require('./../models/servicios');
const Boom = require('boom');

exports.getServicios = (req, res) => {
	const { conductor } = req.query;
	if(!conductor) return res.send(Boom.notBadRequest('Conductor invalido'));
	Servicios.find({ conductor }).populate('pasajero').exec((err, servicios) => {
		if(err) return res.send(err);
		res.send(servicios);
	})
}
