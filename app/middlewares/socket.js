let Channel = {
  connection: (socket) => {
    Channel.socket = socket;
  },
  socket: null
};

module.exports = {
  Channel
}
