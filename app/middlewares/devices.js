const Device = require('./../models/device');
const Conductor = require('./../models/conductor');
const Sender = require('./socket');
const Path = require('path');
const Jsonfile = require('jsonfile');
const	Geolib = require('geolib');
const _ = require('underscore');
const Root = Path.dirname(require.main.filename);
const Boom = require('boom');

const _HandleError = (type, error) => {
	if(type == 'error') {
		console.log(error);
		return Boom.badImplementation(error).output.payload;
	}
}

exports.registroToken = (req, res) => {
		const { registrationId } = req.body;
		let newDevice = new Device({
			registrationId: registrationId
		});

		newDevice.save((err, device) => {
			if (err) return res.send(_HandleError('error', err));
			const data = {
				statusCode: 200,
				value: true,
				mensaje: 'Device registrado exitosamente.',
				id: device._id
			};
			res.send(data);
	});
}

const buscarZona = (latitude, longitude) => {
	const cuadrantes = Jsonfile.readFileSync(Root + '/public/recursos/Cuadrantes.json');
	let zonas = [];

	_.each(cuadrantes.features, (feature) => {
		let coordenadas = [];
		_.each(feature.geometry.coordinates[0], (coordinate) => {
			coordenadas.push({"latitude" : coordinate[0].latitude, "longitude" : coordinate[1].longitude});
		});
		zonas.push({ zona : feature.properties.Name, coordenadas : coordenadas });
	});

	let zona_inside = {};
	_.each(zonas, (item) => {
		if (Geolib.isPointInside({ latitude, longitude }, item.coordenadas)) {
			zona_inside = { zona: item.zona, coordinates: { latitude, longitude } };
			return false;
		}
	});

	return zona_inside;
}

exports.actualizarCoordenadas = (req, res) => {
	const user = req.authenticatedUser;
	const location = req.body;
	if(!user || !location) return res.send(Boom.badImplementation('Parametros no validos').output.payload);

	Conductor.findOne({ _id: user.id.toString()}).exec((err, conductor) => {
		if(err) return res.send(Boom.badImplementation('Error obteniendo datos del conductor').output.payload);
		if(!conductor) return res.send(Boom.notFound('Conductor no encontrado').output.payload);

			Device.findOne({ _id: conductor.device }).exec((err, device) => {
				if(err || !device) return res.send(Boom.notFound('Device no registrado').output.payload);
				device.location = location;
				device.save((err, newDevice) => {
					if (err) return res.send(Boom.badImplementation(err).output.payload);
					const { coordinates } = newDevice.location;
					// const result = buscarZona(-75.4867614, 10.3839119);
					const result = buscarZona(coordinates[1], coordinates[0]);
					const { zona } = result;
					conductor.cuadrante = zona;
					conductor.save();
					if(Sender.Channel && Sender.Channel.socket) {
						Sender.Channel.socket.emit('device', {
							conductor: user.id,
							zona
						});
					}
					res.send(newDevice);
				});
			})
	})
}
