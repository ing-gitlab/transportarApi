
exports.InSession = (req, res, next) => {
	if (req.session.passport.user) {
		res.redirect('/inicio');	
	}
	next();
};

exports.OutSession = (req, res, next) => {
	if (!req.session.passport.user) {
		res.redirect('/login');
	}
	next();
};