const Path = require('path');
const Jsonfile = require('jsonfile');
const	Geolib = require('geolib');
const Boom = require('boom');
const _ = require('underscore');
const Root = Path.dirname(require.main.filename);

const buscarZona = (latitude, longitude) => {
	const cuadrantes = Jsonfile.readFileSync(Root + '/public/recursos/Cuadrantes.json');
	let zonas = [];
	
	_.each(cuadrantes.features, (feature) => {
		let coordenadas = [];
		_.each(feature.geometry.coordinates[0], (coordinate) => {
			coordenadas.push({"latitude" : coordinate[0].latitude, "longitude" : coordinate[1].longitude});
		});
		zonas.push({ zona : feature.properties.Name, coordenadas : coordenadas });
	});

	let zona_inside = {};
	_.each(zonas, (zona) => {
		if (Geolib.isPointInside({ latitude, longitude }, zona.coordenadas)) {
			zona_inside = { zona: zona.zona, coordinates: { latitude, longitude } };
			return false;
		}
	});

	return zona_inside;
}

exports.isInside = (req, res) => {
	const { lat, long } = req.params;
	const zona = buscarZona(lat, long);
	if (Object.keys(zona).length <= 0) return res.send(Boom.notFound('no esta en ninguna zona').output.payload);
	res.send(zona);	
};