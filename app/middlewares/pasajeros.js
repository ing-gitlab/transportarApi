const Pasajero = require('./../models/pasajeros');
const Servicio = require('./../models/servicios');
const Conductor = require('./../models/conductor');
const Device = require('./../models/device');
const validator = require('validator');
const Boom = require('boom');
const jwt = require('jsonwebtoken');
const config = require('getconfig');
const request = require('request');
const moment = require('moment');

const _Validate = (params) => {
	var validate = { isValid: true };
	const { nombres, apellidos, correo, telefono, direccion, conductor } = params;

	if (nombres == "") validate = { isValid: false, message: '¡Escribe tus nombres!' }
	if (apellidos == "") validate = { isValid: false, message: '¡¡Escribe tus apellidos!!' }
	if (correo == "" || !validator.isEmail(correo)) validate = { isValid: false, message: '¡¡Escribe el correo correctamente!!' }
	if (telefono == "" || !validator.isNumeric(telefono)) validate = { isValid: false, message: '¡¡Digita el telefono correctamente!!' }
	if (!direccion || direccion.length == 0)  validate = { isValid: false, message: '¡¡Debes escoger una dirección valida!!' }
	if(!conductor) validate = { isValid: false, message: '¡Debes seleccionar un conductor!' }

	return validate;
}

const _GenerateToken = (_id, secretKey, expiration) => {
	return jwt.sign({ id: _id }, secretKey, { expiresIn: expiration });
}

const sendFirebaseMessage = (url, key, registrationId, servicio) => {
  Servicio.findOne({ _id: servicio }).populate('pasajero').exec((err, servicio) => {
		if(err) return res.send(err);
    const options = {
      method: 'POST',
      url: url,
      headers:{
        authorization: 'key=' + key,
        'content-type': 'application/json'
      },
      body: {
        data: servicio,
        to: registrationId
      },
      json: true
    };
    request(options, (error, response, body) => {
      if (error) throw new Error(error);
      return 'broadcast generated';
    });
	})
}

exports.registroPasajero = (req, res) => {
	const { nombres, apellidos, correo, telefono, direccion, referencia, conductor } = req.body;
	const result = _Validate({ nombres, apellidos, correo, telefono, direccion, referencia, conductor });
	if (!result.isValid) return res.send(Boom.badRequest(result.message).output.payload);
	const location = { "type": "Point", "coordinates": direccion };
	const newPasajero = new Pasajero({
		nombres,
		apellidos,
		correo,
		telefono,
		location,
		referencia,
		conductor
	});

	newPasajero.save((err, responsePasajero) => {
		if (err) return res.send(Boom.badImplementation(err).output.payload);
		Conductor.findOne({ _id: conductor }).exec((e, responseConductor) => {
			if(e) return res.send(Boom.badImplementation(err).output.payload);
			const newServicio = new Servicio({
				pasajero : responsePasajero._id,
				conductor : responseConductor._id,
				location,
				fecha : moment(new Date()).format('MMMM Do YYYY, h:mm a'),
				estado: "Activo"
			})

			newServicio.save((error, responseServicio) => {
				if(error) return res.send(Boom.badImplementation(error).output.payload);
				Device.findOne({ _id: responseConductor.device }).exec((deviceError, responseDevice) => {
					const { sendUrl, serverKey } = config.firebase;
					sendFirebaseMessage(sendUrl, serverKey, responseDevice.registrationId, responseServicio._id)
					const msg = responsePasajero.nombres.split(' ')[0] + ' ha sido registrado exitosamente.';
					res.send({
						statusCode: 200,
						pasajero: responsePasajero,
						message: msg,
						token: _GenerateToken(responsePasajero._id, config.server.secretKey, config.server.tokenExpiration)
					});
				})
			})
		})
	});
};
