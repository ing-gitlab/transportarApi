const Conductor = require('./../models/conductor');
const Servicios = require('./../models/servicios');
const Pasajeros = require('./../models/pasajeros');
const async = require('async');
const validator = require('validator');
const passwordHash = require('password-hash');
const Boom = require('boom');
const jwt = require('jsonwebtoken');
const config = require('getconfig');
const request = require("request");

const _Validate = (params) => {
	var validate = { isValid: true };
	const { nombres, apellidos, telefono, cuadrante, correo, password, confirmPassword } = params;
	if (nombres == "") validate = { isValid: false, message: '¡Escribe tus nombres!' }
	if (apellidos == "") validate = { isValid: false, message: '¡¡Escribe tus apellidos!!' }
	if (correo == "" || !validator.isEmail(correo)) validate = { isValid: false, message: '¡¡Escribe el correo correctamente!!' }
	if (telefono == "" || !validator.isNumeric(telefono)) validate = { isValid: false, message: '¡¡Digita el telefono correctamente!!' }
	if (password == "" || confirmPassword == "") validate = { isValid: false, message: '¡La contraseña es requerida!' }
	if (!validator.equals(password, confirmPassword)) validate = { isValid: false, message: '¡Las contraseñas no coinciden' }
	return validate;
}

const _GenerateToken = (id, secretKey, expiresIn) => jwt.sign({ id }, secretKey, { expiresIn });

const _HandleError = (type, error) => {
	if(type == 'error') {
		console.log(error);
		return Boom.badImplementation(error).output.payload;
	}

	if(type == 'conductor.found') {
		return Boom.badRequest('Ya existe un conductor registrado con este email').output.payload;
	}
}

exports.registroConductor = (req, res) => {
	const { nombres, apellidos, telefono, cuadrante, correo, password, confirmPassword, deviceId } = req.body;
	const entity = { nombres, apellidos, telefono, cuadrante, correo, password, confirmPassword };
	var result = _Validate(entity);
	if (!result.isValid) return res.send(Boom.badRequest(result.message).output.payload);

	Conductor.find({ correo: {'$regex': '.*' + correo + '.*'}}).exec((err, response) => {
			if(err) return res.send(_HandleError('error', err));
			if(response.length > 0) return res.send(_HandleError('conductor.found', err));

			let newConductor = new Conductor({
				nombres: nombres,
				apellidos: apellidos,
				telefono: telefono,
				cuadrante: cuadrante,
				correo: correo,
				password: passwordHash.generate(password),
				device: deviceId
			});

			newConductor.save((err, conductor) => {
				if (err) return res.send(_HandleError('error', err));
				const data = {
					statusCode: 200,
					conductor,
					mensaje: 'Conductor registrado exitosamente.',
					token: _GenerateToken(conductor._id, config.server.secretKey, config.server.tokenExpiration)
				};
				res.send(data);
			});
	});
};

exports.getConductores = (req, res, next) => {
	Conductor.find().exec((err, conductores) => {
		if(err) return res.send(err);
		res.send(conductores);
	})
};

exports.getServicios = (req, res) => {
	const { conductor } = req.params;
	if(!conductor) return res.send(Boom.notBadRequest('Conductor invalido'));
	Servicios.find({ conductor }).populate('pasajero').exec((err, servicios) => {
		if(err) return res.send(err);
		res.send(servicios);
	})
}
