const config = require('getconfig');
const jwt = require('jsonwebtoken');
const expressJwt = require('express-jwt');
const Boom = require('boom');
const async = require('async');

exports.authorize = (req, res, next) => {
  const { authorization } = req.headers;
  if (!authorization) return res.send(Boom.badRequest('Authorization header required').output.payload);
  const split = authorization.split(' ');
  if (split.length !== 2) return res.send(Boom.badRequest('Invalid Authorization header').output.payload);
  const token = split[1];
  jwt.verify(token, config.server.secretKey, (err, decoded) => {
    if (err) return res.send(Boom.unauthorized('Invalid access token').output.payload);
    req.authenticatedUser = decoded;
    return next();
  });
};
