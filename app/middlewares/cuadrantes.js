const Cuadrante = require('./../models/cuadrantes');

exports.getCuadrantes = (req, res, next) => {
	Cuadrante.find({}).exec((err, cuadrantes) => {
		if (err) { console.log(err) };
		req.body.cuadrantes = cuadrantes;
		res.send(cuadrantes);
	});	
};
