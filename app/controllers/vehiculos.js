const Vehiculos = require('./../middlewares/vehiculos');

module.exports = (app) => {
	app.get('/get/vehiculo', Vehiculos.getVehiculo);
};