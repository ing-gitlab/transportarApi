const Cuadrantes = require('./../middlewares/cuadrantes');

module.exports = (app) => {
	app.get('/get/cuadrantes', Cuadrantes.getCuadrantes);
};