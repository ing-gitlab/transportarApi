const Devices = require('./../middlewares/devices');
const Authorization = require('./../middlewares/jwt');

module.exports = (app, socket) => {
	app.post('/registro/token', Devices.registroToken);
	app.put('/coordenadas', Authorization.authorize, Devices.actualizarCoordenadas);
}
