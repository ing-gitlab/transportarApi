const Servicios = require('./../middlewares/servicios');

module.exports = (app) => {
	app.get('/get/servicios', Servicios.getServicios);
}
