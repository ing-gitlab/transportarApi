const Conductores = require('./../middlewares/conductores');

module.exports = (app) => {
	app.post('/registro/conductor', Conductores.registroConductor);
	app.get('/get/conductores', Conductores.getConductores);
	app.get('/get/servicios/:conductor', Conductores.getServicios);
}
