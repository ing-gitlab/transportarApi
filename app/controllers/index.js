const Cuadrantes = require('./../middlewares/cuadrantes');
const Session = require('./../middlewares/session');
const Passport = require('passport');
const Conductor = require('../middlewares/conductores');
const jwt = require('../middlewares/jwt');

module.exports = (app) => {
	app.get('/', Session.InSession, (req, res) => {
		res.redirect('/inicio');
	});

	app.get('/login', Session.InSession, Cuadrantes.getCuadrantes, (req, res) => {
		res.render('login',{
			cuadrantes : req.body.cuadrantes
		});
	});

	app.get('/inicio', Session.OutSession, (req, res) => {
		res.render('index', {
			usuario : req.session.passport.user
		});
	});

	app.get('/horarios', Session.OutSession, (req, res) => {
		res.render('horario', {
			usuario : req.session.passport.user
		});
	});

	app.get('/notificaciones',  (req, res) => {
		res.render('notificaciones', {
			usuario : req.session.passport.user
		});
	});

	app.get('/logout', (req, res) => {
		req.session.destroy();
		req.logOut();
		res.redirect('/login');
	});

	app.post('/login',Passport.authenticate('user', {
			session: false
		}), jwt.authorize, (req, res) => {
			const statusCode = 200;
			res.status(statusCode).json({
				statusCode,
				user: req.user
			})
		}
	);

	// app.post('/login',Passport.authenticate('user', {
	// 		successRedirect: '/inicio',
	// 		failureRedirect: '/error'
	// 	})
	// );

	app.get('/error', (req, res) => {
		res.send(req.flash('error'));
	});
};
