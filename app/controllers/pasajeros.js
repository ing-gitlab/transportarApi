const Pasajeros = require('./../middlewares/pasajeros');

module.exports = (app) => {
	app.post('/registro/pasajero', Pasajeros.registroPasajero);
};