const Zonas = require('./../middlewares/zonas');

module.exports = (app) => {
	app.get('/isInside/:lat/:long', Zonas.isInside);
};