const Cuadrante = require('../models/cuadrantes');
const async = require('async');
const config = require('getconfig');

const crearCuadrantes = (item, callback) => {
	async.waterfall([
		cb => {
			const { nombre, direccion, location } = item;
			let cuadrante = new Cuadrante({
				nombre,
				direccion,
				location
			});

			Cuadrante.findOne({ nombre, direccion, location}).exec(function (err, cuadrantes){
				if (err) return cb(err);
				if(!cuadrantes) {
					cuadrante.save((err, newCuadrante) => {
						if(err) return cb({ error: err, item  });
						cb(null, newCuadrante);
					});
				} else {
					cb();
				}
			});
		}
	], (err, results) => {
		if(err) return callback(err);
		callback(null, results);
	});
}

module.exports = () => {
	async.map(config.cuadrantes, crearCuadrantes, (err, results) => {
		if(err) console.log(err);
		console.log('cuadrantes (loaded)');
	});
}