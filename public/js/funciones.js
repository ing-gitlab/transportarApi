

$(document).ready(function(){
	cargar_mapa();
	$('#menu_inicio').attr('class', 'active');
});


function cargar_mapa(){

	var style_array_from_above_here=[
	{
		"featureType": "landscape",
		"stylers": [
		{
			"saturation": -100
		},
		{
			"lightness": 65
		},
		{
			"visibility": "on"
		}
		]
	},
	{
		"featureType": "poi",
		"stylers": [
		{
			"saturation": -100
		},
		{
			"lightness": 51
		},
		{
			"visibility": "simplified"
		}
		]
	},
	{
		"featureType": "road.highway",
		"stylers": [
		{
			"saturation": -100
		},
		{
			"visibility": "simplified"
		}
		]
	},
	{
		"featureType": "road.arterial",
		"stylers": [
		{
			"saturation": -100
		},
		{
			"lightness": 30
		},
		{
			"visibility": "on"
		}
		]
	},
	{
		"featureType": "road.local",
		"stylers": [
		{
			"saturation": -100
		},
		{
			"lightness": 40
		},
		{
			"visibility": "on"
		}
		]
	},
	{
		"featureType": "transit",
		"stylers": [
		{
			"saturation": -100
		},
		{
			"visibility": "simplified"
		}
		]
	},
	{
		"featureType": "administrative.province",
		"stylers": [
		{
			"visibility": "off"
		}
		]
	},
	{
		"featureType": "water",
		"elementType": "labels",
		"stylers": [
		{
			"visibility": "on"
		},
		{
			"lightness": -25
		},
		{
			"saturation": -100
		}
		]
	},
	{
		"featureType": "water",
		"elementType": "geometry",
		"stylers": [
		{
			"hue": "#ffff00"
		},
		{
			"lightness": -25
		},
		{
			"saturation": -97
		}
		]
	}
	];
	var mapOptions = {
		scrollwheel:false,
		center: new google.maps.LatLng(10.415475,-75.526128),
		zoom: 14,
		mapTypeId: google.maps.MapTypeId.RELIEVE,
		styles:style_array_from_above_here
	};

	var map = new google.maps.Map(document.getElementById("mapa"),
		mapOptions);

}
