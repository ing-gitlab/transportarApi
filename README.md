Vagrant box
> vagrant init bento/ubuntu-16.04; vagrant up --provider virtualbox

Provisions

config.vm.provision "shell", inline: <<-SHELL
  sudo apt-get update
  sudo apt-get purge nodejs npm
  curl -sL https://deb.nodesource.com/setup_6.x -o nodesource_setup.sh
  sudo bash nodesource_setup.sh
  sudo apt-get install -y nodejs
  sudo apt-get install -y build-
  sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
  echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
  sudo apt-get update
  sudo apt-get install -y mongodb-org
SHELL

Install and configure mongodb in vagrant

Go to cd/Vagrant then run the following commands (this 4 commands were added in vagrant provisions)
> sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
> echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
> sudo apt-get update
> sudo apt-get install -y mongodb-org

Create service file to handle mongodb service
> sudo nano /etc/systemd/system/mongodb.service

Copy this in its content

[Unit]
Description=High-performance, schema-free document-oriented database
After=network.target

[Service]
User=mongodb
ExecStart=/usr/bin/mongod --quiet --config /etc/mongod.conf

[Install]
WantedBy=multi-user.target

Then save the file. (ctrl + x , Y, Enter).

Now you should be able to control the service
> sudo systemctl start mongodb

> sudo systemctl status mongodb

> sudo systemctl stop mongodb

more info: https://www.digitalocean.com/community/tutorials/como-instalar-mongodb-en-ubuntu-16-04-es


Install and run redis

Go to cd/Vagrant then run the following commands
> curl -O http://download.redis.io/redis-stable.tar.gz
> tar xzvf redis-stable.tar.gz
> cd redis-stable
> make
> sudo apt-get install -y tcl8.5
> sudo make install
> cd utils/
> sudo ./install_server.sh (in this command the configuration will ask some question, we can do enter to all and at the end you will see redis service started)
> sudo service redis_6379 status | start | stop

See ubuntu version
> lsb_release -a
