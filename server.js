// const kue = require('kue');
const express  = require('express');
const swig     = require('swig');
const passport = require('passport');
const flash 	 = require('connect-flash');
const app    	 = express();
const server 	 = require('http').createServer(app);
const register = require('./app/routes/register');
const config   = require('getconfig');
const io = require('socket.io')(server);
const sender = require('./app/middlewares/socket');

app.engine('.html', swig.renderFile);
app.set('view engine', 'html');
app.set('views', './app/views');

//Allow cross domain
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.configure(() => {
  // app.use('/queue', kue.app);
	app.use(express.logger());
	app.use(express.bodyParser());
	app.use(express.cookieParser());
	app.use(express.static(__dirname + '/public'));
	app.use(express.methodOverride());
	app.use(express.session({ secret: config.server.secretKey }));
	app.use(passport.initialize());
	app.use(passport.session());
	app.use(flash());
	app.use(app.router);
	register(app);
});

const port = Number(process.env.PORT || 5000);
server.listen(port, () => {
  io.on('connection', (socket) => {
    sender.Channel.connection(socket);
    console.log('socket connection established successfully');
  });
	console.log("Server listening on", port);
});
